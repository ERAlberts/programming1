# Does the amount of Dengue cases have a correlation with the amount of rain fall on Aruba?
The Dengue virus has been a part of Aruba for a long time. This virus is typically transmitted trough the yellow fever mosquito, onto human. These mosquito's are known to reproduce in standing water. 
The increase of rain can be a cause for more standing water troughout the island. That's why we will be exploring whether the amount of rain fall can influence the amount of dengue cases on Aruba. 

## Data sets
For this project we will be using two different data sets. 
1. Aruba's weather data can be downloaded on their meteorology website: http://www.meteo.aw/climate.php.
2. Total dengue cases per year can be found on the  Pan American Health Organization:https://www.paho.org/data/index.php/en/mnu-topics/indicadores-dengue-en/dengue-nacional-en/257-dengue-casos-muertes-pais-ano-en.html?start=1 .
        
We will be looking specifically at the years 1995 until 2020. 

### Weather data
Each year is stored in a pdf-file with a table containing different weather data for each row. These files are available for the public.

### Dengue data
The PAHO dengue data can be downloaded by selecting all the years needed and clicking on the country of interest. This data is stored as a csv file.

These datasets are small enough to be stored in an average document on the computer. 

## Analysis approach and data visualization
The chosen analysis approach and data visualization are described in the jupyter notebook program. 


## How to use
In order to run this program, the following programs are needed:
1. Camelot-py (can be downloaded using pip): `pip install "camelot-py[cv]"`.
2. GhostScript: https://www.ghostscript.com/download/gsdnld.html (General Public License V9.53.3). 
3. Pandas (can be downloaded using the command prompt): `pip install pandas`. 
4. Jupyter notebook (can be downloaded using pip): `pip install jupyterlab`.
5. Bokeh (can be downloaded using pip): `pip install bokeh` .

